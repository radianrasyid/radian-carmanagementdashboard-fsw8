const mysql = require('mysql')

const koneksi = mysql.createConnection({
    host : 'localhost',
    user: 'root',
    password: '',
    database: 'carRent',
    multipleStatements: true
})

koneksi.connect((err)=>{
    if(err){
        throw err;
    }
    console.log('We are connected to the database, Sir!');
})

module.exports = koneksi;