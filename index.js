const fs = require('fs')
const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const multer = require('multer')
const helpers = require('./helpers')
const session = require('express-session')
const koneksi = require('./database')
const flash = require('connect-flash')
const { json } = require('express/lib/response')
const { resourceUsage } = require('process')
const { parse } = require('path')
const app = express()
const PUBLIC_DIRECTORY = path.join(__dirname, 'public')

const port = process.env.port || 9901;
app.use(express.static('public'))
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended : true}))
app.use(bodyParser.json())
app.use(flash())
// const carPath = path.join(".", "data", "cars.json")
// const readJSON = fs.readFileSync(carPath)
// let cars = JSON.parse(readJSON)

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, 'public/images')
  },
  filename: function(req, file, cb){
    console.log(file);
    cb(null, file.originalname)
  }
})

app.get("/", (req, res)=>{
  let {filter, all, small, medium, large} = req.query;
  const querySQL = 'SELECT * FROM cars'

  koneksi.query(querySQL, (err, rows, field) => {
    if(err){
      return res.json({ message: 'Ada kesalahan', error: err });
    }
    let dataTampil = rows;
    if(filter){
      dataTampil = rows.filter((car) => {
        const filterer = car.nama.toLowerCase().match(filter.toLowerCase()) || car.ukuran.toLowerCase().match(filter.toLowerCase())
        return filterer;
      })
    }
    if(all){
      dataTampil = rows.filter((car) => {
        const filterer = car.ukuran.toLowerCase().match(all.toLowerCase())
        return filterer;
      })
    }
    if(small){
      dataTampil = rows.filter((car) => {
        const filterer = car.ukuran.toLowerCase().match(small.toLowerCase())
        return filterer;
      })
    }
    if(medium){
      dataTampil = rows.filter((car) => {
        const filterer = car.ukuran.toLowerCase().match(medium.toLowerCase())
        return filterer;
      })
    }
    if(large){
      dataTampil = rows.filter((car) => {
        const filterer = car.ukuran.toLowerCase().match(large.toLowerCase())
        return filterer;
      })
    }
    res.render('index', {
      mobil: dataTampil, filter,
      page: "Dashboard",
      page1: "active",
      page2: "",
      user: "Radian Rasyid"
    })
    
  })
})

app.get('/add', (req, res) => {
  res.render('add', {
    dataPilih: '',
    action: "/add",
    page: "Add Cars",
    page2: "active",
    page1: "",
    user: "Radian Rasyid"
  })
})

app.post("/add", (req, res) => {
  let upload = multer({ storage:storage, fileFilter: helpers.imageFilter }).single('cover');
  let{cover} = req.body;
  upload (req, res, function(err){
    console.log(req.body.cover);
    const newData = {
      nama: req.body.nama,
      sewa: parseInt(req.body.sewa),
      ukuran: req.body.ukuran,
      image: req.file.originalname
    }
    const querySql = 'INSERT INTO cars SET ?';
    const baru = JSON.parse(JSON.stringify(newData))
    // jalankan query
    koneksi.query(querySql, newData, (err, rows, field) => {
        // error handling
        if (err) {
            return res.json({ message: 'Gagal insert data!', error: err });
        }

        // jika request berhasil
        res.redirect("/");
    });
  })
})

app.get("/edit/:id", (req, res) => {
  const {id} = req.params;
  const querySQL = 'SELECT * FROM cars'

  koneksi.query(querySQL, (err, rows, field) => {
    if(err){
      return res.json({ message: 'Ada kesalahan', error: err });
    }
    let dataTampil = rows.filter((car) => {
      return car.no === parseInt(id)
    })
    res.render("edit", {
      page: "Edit Cars",
      dataPilih: dataTampil[0],
      action: `edit/${id}`,
      user: "Radian Rasyid"
    })
    
  })
})

app.post("/edit/:id", (req, res) => {
  const {id} = req.params;
  let upload = multer({ storage:storage, fileFilter: helpers.imageFilter }).single('cover');
  upload (req, res, function(err){
    const querySearch = `SELECT * FROM cars WHERE no = ${id}`;
    const queryUpdate = `UPDATE cars SET ? WHERE no = ${id}`
    if (req.fileValidationError) {
      return res.send(req.fileValidationError);
    }
    koneksi.query(querySearch, id, (err, rows, field) => {
      const newData = {
        ...req.body,
        image: req.file ? req.file.originalname : rows[0].image.toString()
      }
      if(rows.length){
        koneksi.query(queryUpdate, [newData, id], (err, rows, field) => {
          res.redirect("/")
        })
      }
    })
  })
})

app.get("/delete/:id", (req, res)=>{
  const {id} = req.params;
  const querySeach = `SELECT * FROM cars WHERE no = ?`
  const queryDelete = `DELETE FROM cars WHERE no = ?`

  koneksi.query(querySeach, req.params.id, (err, rows, field) => {
    // error handling
    if (err) {
        return res.json({ message: 'Ada kesalahan', error: err });
    }

    // jika id yang dimasukkan sesuai dengan data yang ada di db
    if (rows.length) {
        // jalankan query delete
        koneksi.query(queryDelete, req.params.id, (err, rows, field) => {
            // error handling
            if (err) {
                return res.json({ message: 'Ada kesalahan', error: err });
            }

            // jika delete berhasil
            res.redirect("/")
        });
    } else {
        return res.json({ message: 'Data tidak ditemukan!', success: false });
    }
});
})

app.get("/:id", (req, res)=>{
  const {id} = req.params;
  const querySearch = `SELECT * FROM cars WHERE no = ?`;
  koneksi.query(querySearch, id, (err, rows, field) => {
    newImg = `/images/${rows[0].image}`
    res.render("list", {
      cars: rows[0],
      carsImg: newImg,
      page: "List Cars",
      user: "Radian Rasyid",
      page1: "active",
      page2: ""
    })
  })
})

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
})